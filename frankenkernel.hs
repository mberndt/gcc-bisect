#!/usr/bin/env stack
{- stack script
    --resolver lts-13.4
    --package turtle
    --package binary
    --package optional-args
    --package text
    --package relude
    --package microlens
    --package microlens-th
    --package text-binary
-}

{-# LANGUAGE OverloadedStrings,
             DeriveGeneric,
             NoImplicitPrelude,
             GeneralizedNewtypeDeriving,
             TemplateHaskell,
             RankNTypes
#-}

import Turtle
import Relude hiding(init, FilePath, putStrLn, get, put, toText, (<&>))
import Data.Binary
import Data.Text.IO(putStrLn)
import Data.Text.Binary
import Data.Optional
import Data.List((\\))
import Data.Text(lines)
import Control.Monad(forM_)
import Lens.Micro((%~), (.~), (^.), Lens')
import Lens.Micro.TH(makeLenses)

data MyConfig = MyConfig {
  goodCmd :: Text,
  badCmd :: Text,
  testCmd :: Text
} deriving (Show, Generic)

instance Binary MyConfig

data Cmd =
    Init MyConfig
  | Good
  | Bad
  | PrintState
  deriving (Show, Generic)

data SearchInterval a = SearchInterval [a] [a]
  deriving (Show, Generic)

instance Binary a => Binary (SearchInterval a)

data MyState p = MyState {
  _cfg :: MyConfig,
  _searchInterval :: SearchInterval p,
  _knownGood :: [p],
  _knownBad :: [p],
  _unknown :: [p]
} deriving (Show, Generic)

instance Binary p => Binary (MyState p)

instance Binary FilePath where
  put p = let Right x = toText p in put x
  get = fromText <$> get

makeLenses ''MyState


leftHalf :: Lens' (SearchInterval a) [a]
leftHalf f (SearchInterval x y) = fmap (\a -> SearchInterval a y) (f x)

rightHalf :: Lens' (SearchInterval a) [a]
rightHalf f (SearchInterval x y) = fmap (\a -> SearchInterval x y) (f y)

printBadWhenDone state = do
  let SearchInterval l r = state ^. searchInterval
  when (null l && null r) $ do
      putStrLn "Done!"
      putStrLn "The following files are miscompiled by the bad compiler:"
      forM_ (state ^. knownBad) $ \p -> let
        Right x = toText p
        in putStrLn x
      exit ExitSuccess
    
  
step :: Lens' (SearchInterval FilePath) [FilePath] ->
  Lens' (SearchInterval FilePath) [FilePath] ->
  Lens' (MyState FilePath) [FilePath] ->
  IO ()


step badHalf goodHalf putGood = do
  oldState <- decodeFile "./.frankenkernel-state" :: IO (MyState FilePath)
  let state = nextState badHalf goodHalf putGood oldState
  printBadWhenDone state
  let copyAndPrint from p = let
        fromPath = from </> p ;
        toPath = "build-test" </> p
        in do 
          cp fromPath toPath
          putStrLn $ "cp " <> show fromPath <> " " <> show toPath
  changeState oldState state (copyAndPrint "build-bad") (copyAndPrint "build-good")  
  shells (state ^. cfg & testCmd) empty
  encodeFile "./.frankenkernel-state" state
  pure ()

changeState :: (Monoid m, Eq a) => MyState a -> MyState a -> (a -> m) -> (a -> m) -> m
changeState oldState newState copyFromBad copyFromGood = let
  goodFiles x = (x ^. searchInterval . rightHalf) <> (x ^. unknown)
  badFiles x = (x ^. searchInterval . leftHalf) <> (x ^. knownGood)
  in ((goodFiles newState \\ goodFiles oldState) & foldMap copyFromGood) <>
     ((badFiles newState \\ badFiles oldState) & foldMap copyFromBad)

nextState :: Lens' (SearchInterval a) [a] ->
             Lens' (SearchInterval a) [a] ->
             Lens' (MyState a) [a] ->
             MyState a ->
             MyState a
nextState badHalf goodHalf putGood state = let
  split xs = uncurry SearchInterval $ splitAt (length xs `div` 2) xs
  new = state & putGood %~ (++ (state ^. searchInterval . goodHalf))
  in case state ^. searchInterval . badHalf of
    [] -> new & searchInterval .~ (split (new ^. unknown))
    [x] -> new & knownBad %~ (x:)
               & searchInterval .~ SearchInterval (new ^. unknown) []
               & unknown .~ []
    foo -> new & searchInterval .~ split foo

parser =
  subcommand "init" "init" (Init <$> (MyConfig <$>
    (argText "goodCmd" Default <&> (<> " O=build-good")) <*>
    (argText "badCmd" Default <&> (<> " O=build-bad")) <*>
    (argText "testCmd" Default <&> (<> " O=build-test"))
  )) <|>
  subcommand "good" "good" (pure Good) <|>
  subcommand "bad" "bad" (pure Bad) <|>
  subcommand "printState" "printState" (pure PrintState)


good = step rightHalf leftHalf knownGood
bad = step leftHalf rightHalf unknown
printState = do
  state <- decodeFile "./.frankenkernel-state"
  print (state :: MyState FilePath)

sednp :: Pattern Text -> Shell Line -> Shell Line -- like sed -n s/.../.../p, i. e. will print only those lines that match
sednp pat s = do 
  line <- s
  case match pat (lineToText line) of
    (l:_) -> select $ textToLines l
    [] -> empty
      

init :: MonadIO io => MyConfig -> io ()
init cfg = do
  forM_ ["build-good", "build-bad"] $ \dir -> do
    mktree dir -- TODO: use mkdir here?
    cp ".config" (dir </> ".config")
  rm ".config"
  let pat = spaces1 <> "CC" <> spaces1 *> chars1 <> ".o"
  ofiles <- empty & inshell (goodCmd cfg) & sednp pat & strict & fmap lines
  shells (badCmd cfg) empty
  shells "rm -rf build-test" empty          -- TODO: remove this?
  shells "cp -ar build-bad build-test" empty -- I'd like to use cptree here, but it's buggy, bug #343
  liftIO $ putStrLn $ "found " <> show (length ofiles) <> " object files"
  let myState = MyState {
    _cfg = cfg,
    _searchInterval = SearchInterval (map fromText ofiles) [],
    _knownGood = [],
    _knownBad = [],
    _unknown = []
  }
  liftIO $ encodeFile "./.frankenkernel-state" myState
  pure ()

         
main = do
  x <- options "foo" parser
  case x of
    Init cfg -> do
      init cfg
    Good -> good
    Bad -> bad
    PrintState -> printState
